﻿#include <iostream>

class MyStack {
private:
    int CountOfElemnts = 0;
    int *DinamicStack;

public:
    void push(int value)
    {
        if (CountOfElemnts == 0) {
            DinamicStack = new int[1];
        }
        else
        {
            int* tempStack = new int[CountOfElemnts+1];
            for (int i = 0; i < CountOfElemnts; i++)
            {
                tempStack[i] = DinamicStack[i];
            }
            delete(DinamicStack);
            DinamicStack = tempStack;
        }
        DinamicStack[CountOfElemnts] = value;
        CountOfElemnts++;
    }
    int pop()
    {
        if (CountOfElemnts > 0) {
            int valueFromStack = DinamicStack[--CountOfElemnts];
            if (CountOfElemnts == 0) {
                delete(DinamicStack);
            }
            else
            {
                int* tempStack = new int[CountOfElemnts];
                for (int i = 0; i < CountOfElemnts; i++)
                {
                    tempStack[i] = DinamicStack[i];
                }
                delete(DinamicStack);
                DinamicStack = tempStack;
            }
            return valueFromStack;
        }
        else
        {
            std::cout << "Stack is empty \n";
            return 0;
        }
    }
    void printStack()
    {
        std::cout << "stack: ";
        for (int i = CountOfElemnts-1; i >= 0; i--)
        {
            std::cout << DinamicStack[i] << " ";
        }
        std::cout << "\n";
    }
};
int main()
{
    MyStack stack;
    int value;
    stack.push(12);
    stack.printStack();
    stack.push(22);
    stack.printStack();
    value = stack.pop();
    std::cout << "Pop from stack " << value << "\n";
    stack.printStack();
    stack.push(9);
    stack.printStack();
    stack.push(13);
    stack.printStack();
    stack.push(6);
    stack.printStack();
    value = stack.pop();
    std::cout << "Pop from stack " << value << "\n";
    value = stack.pop();
    std::cout << "Pop from stack " << value << "\n";
    value = stack.pop();
    std::cout << "Pop from stack " << value << "\n";
    value = stack.pop();
    std::cout << "Pop from stack " << value << "\n";
    value = stack.pop();
    std::cout << "Pop from stack " << value << "\n";

}